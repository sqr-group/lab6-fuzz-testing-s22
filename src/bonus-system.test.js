import { calculateBonuses } from "./bonus-system.js";
const assert = require("assert");

describe('Calculate Bonuses tests', () => {
    let app;
    console.log("Tests started");

    // Standart

    test('Standard, x < 10000',  (done) => {
        let a = "Standard";
        let b = 9999;
        let bonus = calculateBonuses(a,b);
        assert.equal(bonus, 0.05 * 1);
        done();
    });

    test('Standard, 10000 <= x < 50000',  (done) => {
        let a = "Standard";
        let b = 10000;
        let bonus = calculateBonuses(a,b);
        assert.equal(bonus, 0.05 * 1.5);
        done();
    });

    test('Standard, 50000 <= x < 100000',  (done) => {
        let a = "Standard";
        let b = 50000;
        let bonus = calculateBonuses(a,b);
        assert.equal(bonus, 0.05 * 2);
        done();
    });

    test('Standard, 100000 <= x',  (done) => {
        let a = "Standard";
        let b = 100000;
        let bonus = calculateBonuses(a,b);
        assert.equal(bonus, 0.05 * 2.5);
        done();
    });

    // Premium

    test('Premium, x < 10000',  (done) => {
        let a = "Premium";
        let b = 9999;
        let bonus = calculateBonuses(a,b);
        assert.equal(bonus, 0.1 * 1);
        done();
    });

    test('Premium, 10000 <= x < 50000',  (done) => {
        let a = "Premium";
        let b = 10000;
        let bonus = calculateBonuses(a,b);
        assert.equal(bonus, 0.1 * 1.5);
        done();
    });

    test('Premium, 50000 <= x < 100000',  (done) => {
        let a = "Premium";
        let b = 50000;
        let bonus = calculateBonuses(a,b);
        assert.equal(bonus, 0.1 * 2);
        done();
    });

    test('Premium, 100000 <= x',  (done) => {
        let a = "Premium";
        let b = 100000;
        let bonus = calculateBonuses(a,b);
        assert.equal(bonus, 0.1 * 2.5);
        done();
    });

    // Diamond

    test('Diamond, x < 10000',  (done) => {
        let a = "Diamond";
        let b = 9999;
        let bonus = calculateBonuses(a,b);
        assert.equal(bonus, 0.2 * 1);
        done();
    });

    test('Diamond, 10000 <= x < 50000',  (done) => {
        let a = "Diamond";
        let b = 10000;
        let bonus = calculateBonuses(a,b);
        assert.equal(bonus, 0.2 * 1.5);
        done();
    });

    test('Diamond, 50000 <= x < 100000',  (done) => {
        let a = "Diamond";
        let b = 50000;
        let bonus = calculateBonuses(a,b);
        assert.equal(bonus, 0.2 * 2);
        done();
    });

    test('Diamond, 100000 <= x',  (done) => {
        let a = "Diamond";
        let b = 100000;
        let bonus = calculateBonuses(a,b);
        assert.equal(bonus, 0.2 * 2.5);
        done();
    });

    // Nonsence

    test('Nonsense, x < 10000',  (done) => {
        let a = "Nonsense";
        let b = 9999;
        let bonus = calculateBonuses(a,b);
        assert.equal(bonus, 0);
        done();
    });

    test('Nonsense, 10000 <= x < 50000',  (done) => {
        let a = "Nonsense";
        let b = 10000;
        let bonus = calculateBonuses(a,b);
        assert.equal(bonus, 0);
        done();
    });

    test('Nonsense, 50000 <= x < 100000',  (done) => {
        let a = "Nonsense";
        let b = 50000;
        let bonus = calculateBonuses(a,b);
        assert.equal(bonus, 0);
        done();
    });

    test('Nonsense, 100000 <= x',  (done) => {
        let a = "Nonsense";
        let b = 100000;
        let bonus = calculateBonuses(a,b);
        assert.equal(bonus, 0);
        done();
    });

    console.log('Tests Finished');
});
